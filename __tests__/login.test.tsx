import { render, screen, fireEvent, act } from "@testing-library/react";
import axios from 'axios';
import LoginPage from '../src/app/login/page';
import { successResponse } from '../__mocks__/axios/loginSuccessResponse';

jest.mock('axios');

// Mock utility function for reloading the page
const reloadPage = jest.fn();

// Mock window.location
beforeEach(() => {
  Object.defineProperty(window, 'location', {
    value: { reload: reloadPage },
    writable: true,
  });
});

test("Page: User logs in", async () => {
  // Mock Axios response
  axios.post.mockResolvedValue(successResponse);

  render(<LoginPage />)

  const emailInput = screen.getByLabelText("Email");
  const passwordInput = screen.getByLabelText("Password");
  const loginFormButton = screen.getByRole("button", { name: /Login to concertify/i });

  // Input values
  const emailValue = "fred65n6@stud.kea.dk";
  const passwordValue = "Hej123456!";

  // Fill in the form
  act(() => {
    fireEvent.change(emailInput, { target: { value: emailValue } });
    fireEvent.change(passwordInput, { target: { value: passwordValue } });
  });

  // Submit the form
  act(() => {
    fireEvent.click(loginFormButton);
  });

  // Wait for the asynchronous actions to complete
  await act(async () => {
    // Log the value of document.cookie
    console.log("user logged in");
  });

  // Ensure the reload function was called
  expect(reloadPage).toHaveBeenCalled();
});

