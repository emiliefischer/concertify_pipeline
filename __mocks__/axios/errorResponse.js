// __mocks__/axios/errorResponse.js
export const errorResponse = {
  response: {
    status: 500,
    data: {
      error: 'Internal Server Error',
    },
  },
};

