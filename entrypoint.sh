#!/bin/sh

coverage () {
    coverage_output=$(npm test -- --coverage)

    # Display the full coverage output for debugging
    echo "Coverage Output:"
    echo "$coverage_output"

    # Extract the coverage percentage
    coverage=$(echo "$coverage_output" | awk -F"|" '/All files/{gsub(/[%[:space:]]/, "", $2); print $2}')

    # Display the extracted coverage percentage for debugging
    echo "Extracted Coverage: $coverage%"

    # Compare coverage against the threshold (e.g., 80%)
    if [ -n "$coverage" ] && [ "$(echo "$coverage >= 80" | bc)" -eq 1 ]; then
      echo "Code coverage is $coverage%, which is sufficient."
    else
      echo "Code coverage is $coverage%, which is below the required threshold (80%)."
      exit 1
    fi
}

case "$RTE" in
    dev )
        echo "** Development mode."
        npm audit
        npm run lint
        coverage
        npm run dev
        ;;
    test )
        echo "** Test mode."
        npm audit
        npm run lint
        coverage
        ;;
    prod )
        echo "** Production mode."
        npm audit || exit 1
        npm run build
        ;;
esac
