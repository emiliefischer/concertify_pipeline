// Learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom'

module.exports = {
  // other Jest configurations...
  setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
};
